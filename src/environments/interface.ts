export interface Environment {
  apiKey: string;
  DbUrl: string;
  production: boolean;
  signInUrl: string;
  signUpUrl: string;
}
