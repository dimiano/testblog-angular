import { Environment } from './interface';

export const environment: Environment = {
  production: true,
  apiKey: 'AIzaSyAK-b9koJyFbC7POqBxhmHwe0vIaG2-SIg',
  DbUrl: 'https://testblog-angular.firebaseio.com',
  signInUrl: 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=',
  signUpUrl: 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key='
};
